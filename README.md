### What is this?
Android Devices from BQ (and maybe others) have a write protection on the system partition. Even with root permissions it's not possible to write to the system partition because the device will crash when a write protect violation is detected.  
This script creates an overlay filesystem on top of the system partition. Changes made to /system will be saved on the /data partition. This way apps running with root permissions (e.g. adblockers) will be able to write to /system.  
Currently this has only been tested on a BQ Aquaris X Pro so it might not work for other devices.  

### What does it do exactly?
- Read /proc/mounts to get all mountpoints in /system
- Use bind mounts to bind all the submounts of /system to the /data partition
- Create the overlay on /system (submounts are excluded)
- Use bind mounts to restore all the submounts in /system

### Prerequisites
- Root access.
- The Android kernel has to support overlay filesystems (`CONFIG_OVERLAY_FS=y`).
- The SELinux policy has to consider overlay filesystems capable of labeling (or SELinux has to be set to permissive mode).

### Configuration
You can change the `OVERLAYPATH` variable in the script to change the storage of the overlay changes.

### Limitations
- Spaces in mountpaths are not supported.
- Reverse mounts are not considered. If there exists some bind mount /system/anything -> /other/path and the overlay is created afterwards, this mount will not update and therefore changes that are made in /system/anything will not be visible in /other/path.
- Because changes are stored on /data and not on /system they will not be in effect on boot. Actually they are not in effect at all until the overlay is active. The script has to be executed after every boot to apply the past changes and to be able to write to /system.

### LineageOS Build
With two small changes to the LineageOS source code it's possible to add the necessary prerequisites even if overlayfs is not natively supported.
1. Add `CONFIG_OVERLAY_FS=y` to the kernel config (or change the `CONFIG_OVERLAY_FS` line if already existing)  
The kernel config file is located at `./kernel/<your vendor>/<your device kernel>/arch/<your arch>/configs/<your config>` in the LineageOS source.  
For the BQ Aquaris X Pro this would be `./kernel/bq/msm8953/arch/arm64/configs/bardock_defconfig`
2. Add the necessary SELinux rule for overlayfs  
The concerned file is located at `./system/sepolicy/private/fs_use`  
Add the line `fs_use_xattr overlay u:object_r:labeledfs:s0;` to add overlayfs to the list of SELinux supported filesystems.
