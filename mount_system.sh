#!/system/bin/sh

# set overlay path
OVERLAYPATH='/data/local/system_overlayfs'
G='\033[32m'
R='\033[31m'
E='\033[0m'
DONE="${G}done${E}"
FAILED="${R}failed${E}"
SKIPPED="${R}skipped${E}"

errors_occured=false

# only accept clean binds dir
find ${OVERLAYPATH}/binds > /dev/null
if [[ "$?" -eq "0" ]]; then
	# bind dir exists
	echo -e "${R}The file or directory ${OVERLAYPATH}/binds exists. Please reboot the device and delete it manually afterwards."
	exit 1
fi

# create required directories
mkdir -p ${OVERLAYPATH}
mkdir -p ${OVERLAYPATH}/{binds,upperdir,workdir}

# determine submounts in /system
SYSTEM_MOUNTS=($(cat /proc/mounts | while read -r line ; do
	parts=($line)
	if [[ ${parts[1]} == /system/* ]]; then
		echo ${parts[1]}
	fi
done | sort | uniq))

if [[ ${#SYSTEM_MOUNTS[@]} -ge 1 ]]; then
	echo -e "${G}Found mounts:${E}"
	for found_mount in ${SYSTEM_MOUNTS[@]}; do
		echo $found_mount
	done

	echo ""
else
	echo -e "${G}Did not find any mounts.${E}"
	exit 2
fi

# backup submounts
bind_counter=0
for submount in ${SYSTEM_MOUNTS[@]}; do
	echo -n "Backing up ${submount}..."

	# check mount command for spaces
	mc=$(echo -e $submount)
	mc_array=($mc)
	mc_arraylen=${#mc_array[@]}

	if [[ "$mc_arraylen" -eq "1" ]]; then
		# no spaces, submount is ok
		# check if file or directory
		if [ -d $submount ]; then
			mkdir -p ${OVERLAYPATH}/binds/${bind_counter}
		else
			touch ${OVERLAYPATH}/binds/${bind_counter}
		fi

		mount -o bind $submount ${OVERLAYPATH}/binds/${bind_counter}
		if [[ "$?" -eq "0" ]]; then
			echo -e $DONE
		else
			echo -e $FAILED
			errors_occured=true

		fi
	bind_counter=$((bind_counter+1))

	else
		# mount contains spaces, skip
		echo -e $SKIPPED
		echo -e "${R}Skipping mountpoint '$submount' due to spaces in path. This is not supported.${E}"
		errors_occured=true
	fi
done

if $errors_occured; then
	while true; do

		echo -e "${R}Some errors occured. Do you want to create the overlay anyway?${E}"

		read -r yesno
		case $yesno in
			[Yy]* )
				break;;
			[Nn]* )
				exit;;
			* )
				echo "Answer not understood. Yes/No?";;
		esac
	done
fi

# create main overlay
echo -n "Creating overlay..."
mount -t overlay overlay -o lowerdir=/system,upperdir=${OVERLAYPATH}/upperdir,workdir=${OVERLAYPATH}/workdir /system
if [[ "$?" -eq "0" ]]; then
	echo -e $DONE
else
	echo -e $FAILED
	exit 3
fi

# restore submounts
bind_counter=0
for submount in ${SYSTEM_MOUNTS[@]}; do
	echo -n "Restoring ${submount}..."

	# check mount command for spaces
	mc=$(echo -e $submount)
	mc_array=($mc)
	mc_arraylen=${#mc_array[@]}

	if [[ "$mc_arraylen" -eq "1" ]]; then
		# no spaces, submount is ok
		mount -o bind ${OVERLAYPATH}/binds/${bind_counter} ${submount}

		if [[ "$?" -eq "0" ]]; then
			echo -e $DONE
		else
			echo -e $FAILED
		fi

		umount ${OVERLAYPATH}/binds/${bind_counter}

		if [[ "$?" -eq "0" ]]; then
			if [ -d ${OVERLAYPATH}/binds/${bind_counter} ]; then
				rmdir ${OVERLAYPATH}/binds/${bind_counter}
			else
				rm ${OVERLAYPATH}/binds/${bind_counter}
			fi
		else
			echo -e "${R}The temporary bind mount ${bind_counter} could not be unmounted${E}"
		fi

		bind_counter=$((bind_counter+1))

	else
		# mount contains spaces, trying to escape
		echo -e $SKIPPED
	fi
done

rmdir ${OVERLAYPATH}/binds
